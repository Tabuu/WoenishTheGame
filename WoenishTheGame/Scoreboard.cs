﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoenishTheGame
{
    class Scoreboard
    {
        public Scoreboard(int score, Vector2 locatie)
        {
            Console.SetCursorPosition(locatie.x, locatie.y);
            Console.Write("_________________");

            Console.SetCursorPosition(locatie.x, locatie.y + 1);
            Console.Write("Your score:");

            Console.SetCursorPosition(locatie.x, locatie.y + 2);
            Console.Write(score + "");

            Console.SetCursorPosition(locatie.x, locatie.y + 3);
            Console.Write("_________________");
        }
    }
}
