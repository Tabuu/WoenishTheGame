﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoenishTheGame
{
    class Game
    {
        string _currentInput = "";

        Scoreboard _scoreboard;

        float
            _wordSpeed = 0.2f,
            _timeLeft = 60f,
            _spawnTime = 0f,
            _spawnTimer = 0f;

        bool _isGameInProgress = true;

        int 
            _score,
            _displayScore;

        List<Word> _words;
        FinishLine _finish;
        WordList _wordList;

        ConsoleColor[] _colorList =
        {
            ConsoleColor.Cyan,
            ConsoleColor.Red,
            ConsoleColor.Yellow,
            ConsoleColor.Green,
            ConsoleColor.Blue,
            ConsoleColor.Magenta
        };

        public void Start()
        {
            Console.Title = "Hyper Typer";

            _words = new List<Word>();
            _wordList = new WordList();
            _finish = new FinishLine(99, _words);

            _finish.WordCrossLineEvent += OnWordCrossLine;
        }

        public void Update()
        {
            if ((int)_timeLeft == 0)
            {
                _isGameInProgress = false;
                _scoreboard = new Scoreboard(_score, new Vector2(10, 4));
            }

            if (_isGameInProgress)
            {
                _timeLeft -= 0.05f;
                HandleInput();
                HandleScore();

                _spawnTimer -= 0.05f;
                if (_spawnTimer <= 0)
                {
                    _spawnTimer = _spawnTime;
                    SpawnWord();
                }

                _finish.Update();

                foreach (Word word in _words)
                {
                    word.Update();
                    word.SetSpeed(_wordSpeed);
                }
            }
        }

        void HandleScore()
        {
            if (_score <= 250) _spawnTime = 0.5f;
            if (_score <= 100) _spawnTime = 0.75f;
            if (_score <= 50) _spawnTime = 1f;
            if (_score <= 10) _spawnTime = 1.5f;

            if (_displayScore < _score)
            {
                _displayScore++;
            }
            else if (_displayScore > _score)
            {
                _displayScore--;
            }

            int x = Console.CursorLeft;
            int y = Console.CursorTop;

            Console.SetCursorPosition(0, 0);
            Console.Write("Score: " + _displayScore + "     ");

            Console.SetCursorPosition(100, 0);
            Console.Write("Time left: " + (int)_timeLeft + " ");
            Console.SetCursorPosition(x, y);
        }

        void HandleInput()
        {
            Console.SetCursorPosition(0, 29);
            Console.Write("> " + _currentInput);

            if (Console.KeyAvailable)
            {
                ConsoleKeyInfo keyinfo = Console.ReadKey();
                if (keyinfo.Key == ConsoleKey.Enter)
                {
                    CheckWord(_currentInput);
                    _currentInput = "";
                    Console.SetCursorPosition(0, 29);
                    Console.Write(">                                                                               ");
                }
                else if (keyinfo.Key == ConsoleKey.Backspace)
                {
                    if (_currentInput.Length > 0)
                    {
                        _currentInput = _currentInput.Substring(0, _currentInput.Length - 1);
                        Console.SetCursorPosition(0, 29);
                        Console.Write(">                                                                               ");
                    }
                }
                else
                {
                    _currentInput += keyinfo.KeyChar;
                }
            }
        }

        void OnWordCrossLine(Word word)
        {
            word.Delete();
            _words.Remove(word);
            _score -= word.GetWord().Length;
        }

        void CheckWord(string word)
        {
            foreach (Word wordobj in _words)
            {
                if (word.ToLower().Equals(wordobj.GetWord().ToLower()))
                {
                    wordobj.Delete();
                    _words.Remove(wordobj);
                    _score += wordobj.GetWord().Length;
                    break;
                }
            }
        }

        void SpawnWord()
        {
            Random rnd = new Random();
            int color = rnd.Next(0, _colorList.Length);
            _words.Add(new Word(_wordList.GetWordList()[rnd.Next(0, _wordList.GetWordList().Length)], new Vector2(0, rnd.Next(1, 28)), _colorList[color]));
        }
    }
}
